**This is the GitLabHQ Version of the rainyCloud Git Repository**


## Engineering Guidelines
As we are using open-source software we'll need to adapt it to our needs.
While engineering, please make sure to add rainyCloud functionality and try to make it easy to add it to the Syrenium framework.