# rainyCloud Domain Services / Directory Services
Directory of rainyCloud's Domain Services / Directory Services.

## What are they?
Ever wanted to connect all of your devices to a central network of yours? Well, now you can.

Use...
> rainyCloud MDM for Mobile Device Management (iOS, iPadOS, Android, tvOS, macOS)
> rainyCloud Inactive Directory for Desktop, Laptop and Hybrid Device Management (Windows, Linux, macOS, rainOS, ...)